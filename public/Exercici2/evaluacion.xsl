<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <body>
                <h1>M04 - Notas</h1>
                <table style="border: 1px solid black">
                    <tr style="background-color:lightgreen">
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Telefono</th>
                        <th>Repetidor</th>
                        <th>Nota Practica</th>
                        <th>Nota Examen</th>
                        <th>Nota Total</th>
                        <th>Avatar</th>
                    </tr>
                    <xsl:for-each select="evaluacion/alumno">
                        <xsl:sort select="apellidos" order="ascending"></xsl:sort>
                        <tr>
                            <th>
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </th>
                            <th>
                                <xsl:value-of select="apellidos"></xsl:value-of>
                            </th>
                            <th>
                                <xsl:value-of select="telefono"></xsl:value-of>
                            </th>
                            <th>
                                <xsl:value-of select="@repite"></xsl:value-of>
                            </th>
                            <th>
                                <xsl:value-of select="notas/practicas"></xsl:value-of>
                            </th>
                            <th>
                                <xsl:value-of select="notas/examen"></xsl:value-of>
                            </th>
                            <xsl:choose>
                                <xsl:when test="(notas/practicas+notas/examen) div 2 &lt; 5">
                                    <th style="background-color:red">
                                        <xsl:value-of select="(notas/practicas+notas/examen) div 2"></xsl:value-of>
                                    </th>
                                </xsl:when>
                                <xsl:when test="(notas/practicas+notas/examen) div 2 &gt; 8">
                                    <th style="background-color:lightblue">
                                        <xsl:value-of select="(notas/practicas+notas/examen) div 2"></xsl:value-of>
                                    </th>
                                </xsl:when>
                                <xsl:otherwise>
                                    <th style="background-color:grey">
                                        <xsl:value-of select="(notas/practicas+notas/examen) div 2"></xsl:value-of>
                                    </th>
                                </xsl:otherwise>
                            </xsl:choose>
                            <td>
                                <img style="width:50px; height:50px;">
                                    <xsl:choose>
                                        <xsl:when test="foto">
                                            <xsl:attribute name="src"> imagenes/alumnos/<xsl:value-of select="foto" />
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="src">imagenes/default.png</xsl:attribute>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </img>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>