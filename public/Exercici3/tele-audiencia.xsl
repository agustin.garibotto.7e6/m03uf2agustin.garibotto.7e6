<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/programacio">
<cadena>
    <nom>Un TV</nom>
    <programas>
        <xsl:apply-templates/>
    </programas>
</cadena>
</xsl:template>

<xsl:template match="audiencia">
<programa>
    <xsl:attribute name="hora">
        <xsl:value-of select="hora"></xsl:value-of>
    </xsl:attribute>
    <nom-programa><xsl:value-of select="cadenes/cadena[2]/text()"></xsl:value-of></nom-programa>
    <audiencia><xsl:value-of select="cadenes/cadena[2]/@percentatge"></xsl:value-of></audiencia>
</programa>
</xsl:template>
</xsl:stylesheet>